Книга про Git:

https://git-scm.com/book/ru/v2

основні команди git:

git status
// Створити новий репозиторій
git init
// Скопіювати існуючий репозиторій
git clone https://github.com/akadatsky/HillelJavaHomeworks

git add filaneme
git add .
git commit
git commit -m "My commit message"

// Додати сервер куди відправлятимуть бекапи під умовним ім'ям "origin"
git remote add origin https://github.com/akadatsky/HillelJavaHomeworks.git

// вивести список доданих серверів
git remote -v

// створити гілку homework2 і перейти до неї
git checkout -b homework2

// перейти в існуючу гілку
git checkout master

// вивести список гілок
git branch -v

// відправити всі коміти з поточної гілки в гілку master на сервер
// параметр -u означає upstream: запам'ятати куди ми відправили
// і надалі при команді "git push" відправляти туди ж
git push -u origin master
git push

// перейти у гілку homework2 і відправити її на сервер у гілку homework2:
git checkout homework2
git push -u origin homework2

// завантажити оновлення з сервера для поточної гілки
git pull

Як створити SSH ключ:
https://docs.github. com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent

Як додати SSH ключ на github:
https:// docs.github.com/en/authentication/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account
